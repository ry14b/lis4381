> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Rachel Yopp

### Assignment 5 Requirements:

*Sub-Heading:*

1. Bitbucket read only access.
2. README.md.
3. Blackboard links.

#### README.md file should include the following items:

* Screenshot of DB
* Screenshot of error server side validation



#### Assignment Screenshots:

*Screenshot of Database*:

![Database](img/db.png)

*Screenshot of Server Side Error*:

![Failed validation](img/error.png)



