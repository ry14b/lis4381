> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Rachel Yopp

### Project 1 Requirements:

*Sub-Heading:*

1. Bitbucket read only access.
2. README.md.
3. Blackboard links.

#### README.md file should include the following items:

* Screenshot of 1st screen application
* Screenshot of 2nd screen application


#### Assignment Screenshots:


*Screenshot of Screen 1*:

![Screen 1 Screenshot](img/p1screen1.png)

*Screenshot of Screen 2*:

![Screen 2 Screenshot](img/p1screen2.png)


