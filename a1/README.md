> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Rachel Yopp

### Assignment 1 Requirements:

*Sub-Heading:*

1. Bitbucket read only access.
2. README.md.
3. Blackboard links.

#### README.md file should include the following items:

* Screenshot of ampps install.
* Screenshot of running JDK. 
* Screenshot of my first Andriod app.
* git comands.

> This is a blockquote.
>
> This is the second paragraph in the blockquote.
> #### Git commands w/short descriptions:

1. git init creates an empty Git repository, or reinitializes an existing one 
2. git status shows you the status of your working tree so that you can determine if you need to commit or pull your changes
3. git add is used to add file contents to the index and prepares the content stged for the next commit
4. git commit records the changes to the repository and allows you to commit your changes with a message for reference
5. git push updates the local and shared repo with commited changes
6. git pull fetches and integrates with another shared repo or local branch
7. git merge joins two or more developed projects together

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/ampps.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/android.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/ry14b/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/ry14b/myteamquotes/ "My Team Quotes Tutorial")
