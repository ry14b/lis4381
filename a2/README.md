> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Rachel Yopp

### Assignment 2 Requirements:

1. Screenshot of first user interface
2. Screenshot of second user interface
3. +10 points different background color

#### README.md file should include the following items:

* Screenshot of running applications first user interface.
* Screenshot of running applications second user interface.

#### Assignment Screenshots:

*Screenshot of first user interface:*

![First User Interface](img/interface1.png)

*Screenshot of second user interface*:

![Second User Interface](img/interface2.png)
