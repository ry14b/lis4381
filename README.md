> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Rachel Yopp

### Assignments:

1. [A1 README.md](a1/README.md "A1 README.md file")
	- Install AMPPS, JDK, and Android Studio
	- Create Bitbucket Repo
	- Git Commands with Descriptions

2. [A2 README.md](a2/README.md "A2 README.md file")
	- Screenshot of first user interface
	- Screenshot of second user interface
	- +10 points different background color

3. [A3 README.md](a3/README.md "A3 README.md file")
	- Screenshot of ERD
	- Screenshot of 1st screen application
	- Screenshot of 2nd screen application

4. [P1 README.md](p1/README.md "P1 README.md file")
	- Screenshot of 1st screen application
	- Screenshot of 2nd screen application

5. [A4 README.md](a4/README.md "A4 README.md file")
	- Screenshot of home page
	- Screenshot of A4 failed validation
	- Screenshot of A4 passed validation

6. [A5 README.md](a5/README.md "A5 README.md file")
	- Screenshot of DB
	- Screenshot of error server side validation

7. [P2 README.md](p2/README.md "P2 README.md file")
	- Screenshot of error
	- Screenshot of edit page
	- Screenshot of carousel
	- Screenshot of rss
	- Screenshot of p2


