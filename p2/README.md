> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Rachel Yopp

### Project 2 Requirements:

*Sub-Heading:*

1. Bitbucket read only access.
2. README.md.
3. Blackboard links.

#### README.md file should include the following items:

* Screenshot of Error
* Screenshot of Edit Page
* Screenshot of Carousel
* Screenshot of RSS
* Screenshot of P2



#### Assignment Screenshots:

*Screenshot of Error*:

![Error](img/error.png)

*Screenshot of P2*:

![P2 Update](img/p2.png)

*Screenshot of Carousel*:

![Carousel](img/home.png)

*Screenshot of Edit Page*:

![Carousel](img/update.png)

*Screenshot of RSS Feed*:

![Carousel](img/rss.png)



