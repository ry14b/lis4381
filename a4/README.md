> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Rachel Yopp

### Assignment 4 Requirements:

*Sub-Heading:*

1. Bitbucket read only access.
2. README.md.
3. Blackboard links.

#### README.md file should include the following items:

* Screenshot of home page
* Screenshot of A4 failed validation
* Screenshot of A4 passed validation


#### Assignment Screenshots:

*Screenshot of Carousel*:

![Home Page](images/home.png)

*Screenshot of Screen 1*:

![Failed validation](images/failed.png)

*Screenshot of Screen 2*:

![Passed validation](images/passed.png)



